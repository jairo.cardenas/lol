<header>
<div class="container">
    <div class="row animated fadeInDown">
        <div class="col-md-9">
            <img src="/medicnow/public/img/logoIzquierda.png" alt="El logo" width="25%" />
        </div>
        <div class="col-md-3">
            <div class="text-right">
                <a href="/medicnow/module/acceso/view/logueo.php" class="btn btn-primary">Iniciar sesión</a>
                             
            </div>
            <div style="padding-top: 30px;">

                <img src="/medicnow/public/img/logoDerecha.png" alt="El logo" width="100%" />
            </div>                    
        </div>
    </div>
    <nav class="navbar navbar-expand-sm bg-info navbar-dark animated fadeInLeft ">

        <a class="navbar-brand" href="#">Medic Now</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/medicnow/public/index.php">Inicio</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/medicnow/module/medicnow/view/sedes.php">Sedes</a>
                </li>
                <li class="nav-item" style="color:#0099ff ">
                    <a class="nav-link" href="/medicnow/module/medicnow/view/noticias.php">Novedades</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/medicnow/module/medicnow/view/acerca_de.php">Acerca de</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/medicnow/module/medicnow/view/contactenos.php">Contáctenos</a>
                </li>

            </ul>
        </div>
    </nav>
</div>
</header>





