<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php
        require_once './icono.php';
        ?>
        <meta charset="UTF-8">
        <title>Pagína de bienvenida</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/fontawesome-all.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/animate.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/jquery-ui.js" type="text/javascript"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
    </head> 


    <body>


        <!--//**************************************Cabeza**********************************-->

        <br>
        <br>

        <div class="jumbotron container ">
            <h1 class="display-4">Parcial segundo corte</h1>
            <p class="lead">Jairo Alberto Cardenas Molina</p>
            <hr class="my-4">
            <p>De clic en empezar</p>

            <div class="row">
                <div class="col-sm-1">
                    <p class="lead">
                        <a class="btn btn-info btn-lg" href="/lol/module/admin/view/personaje_crear.php" role="button">Crear</a>
                    </p>
                </div>

                <div class="col-sm-2">
                    <p class="lead">
                        <a class="btn btn-primary btn-lg" href="/lol/module/admin/view/personaje_listar.php" role="button">Listar</a>
                    </p>

                </div>
            </div> 




        </div>


        <?php
        require_once '../public/footer.php';
        ?>
    </div>
</section>
</body>
</html>
