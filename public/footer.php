<!-- Footer -->
<footer id="pie_pagina" class="page-footer font-small blue">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        © Devopss<br>
        Jairo Alberto Cardenas Molina <br>
        <a href="https://mail.google.com/mail/u/0/#inbox?compose=new"> jairo.cardenas@usantoto.edu.co</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->


