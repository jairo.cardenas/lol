
CREATE USER 'user_conn'@'localhost' IDENTIFIED BY '123456';
GRANT ALL PRIVILEGES ON *.* TO 'user_php'@'localhost';
FLUSH PRIVILEGES; 

create DATABASE bd_lol character set utf8 collate utf8_general_ci;
use bd_lol;


CREATE TABLE personaje(
  idpersonaje INT AUTO_INCREMENT,
  nombre VARCHAR(50) NOT NULL,
  rol VARCHAR(50) NOT NULL,
  habilidad VARCHAR(50) NOT NULL,
  descripcion VARCHAR(500) NOT NULL,
  PRIMARY KEY (idpersonaje)
);


INSERT INTO personaje(nombre, rol, habilidad, descripcion) values ( 'AHRI', 'mago',  'ROBO DE ESENCIAS', 'Ahri es una vastaya conectada de forma innata al poder latente de Runaterra' );
INSERT INTO personaje(nombre, rol, habilidad, descripcion) values ( 'ASHE', 'tirador',  'TIRO CONGELADOR', 'Ashe, comandante hija del hielo de la tribu de Avarosa, lidera las hordas más numerosas del norte');
INSERT INTO personaje(nombre, rol, habilidad, descripcion) values ('CASSIOPEIA', 'mago',  'ELEGANCIA SERPENTINA', 'Cassiopeia es una criatura mortal empeñada en manipular a otros según su siniestra voluntad');
