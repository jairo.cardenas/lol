<?php

namespace Module\Admin\Repository;

use PDO;
use Config\Conn;
use Module\Admin\Model\Personaje;


class PersonajeRepository extends Conn {

    public function getAll() {
        $fields = "p.nombre, p.rol, p.habilidad, p.descripcion ";

        $sql = "SELECT  " . $fields;
        $sql .= "FROM  personaje p ";
        $sql .= " ORDER BY p.nombre Asc;";

        $resource = $this->_conn->prepare($sql);
        $resource->execute();
        $rows = $resource->fetchAll(PDO::FETCH_ASSOC);
        
        
//        echo "<pre>";
//        print_r($rows);
//        echo "</pre>";
//       exit();
//        
        return $rows;
    }

    public function add(Personaje $entity) {
        $sql = "INSERT INTO personaje(idpersonaje , nombre , rol , habilidad , descripcion ) ";
        $sql .= " VALUES (?, ?, ?, ?, ?)";
        $resource = $this->_conn->prepare($sql);

//        var_dump($entity);

        $idpersonaje  = $entity->get_idpersonaje();
        $nombre = $entity->get_nombre();
        $rol = $entity->get_rol();
        $habilidad = $entity->get_habilidad();
        $descripcion = $entity->get_descripcion();
       

        $resource->bindParam(1, $idpersonaje);
        $resource->bindParam(2, $nombre);
        $resource->bindParam(3, $rol);
        $resource->bindParam(4, $habilidad);
        $resource->bindParam(5, $descripcion);
   

        $resource->execute();

//        echo "<pre>";
//        $resource->debugDumpParams();
//        echo "</pre>";

        return $resource;
    }

   
}

?>