<?php //
//require_once '../../../config/loader.php';
//
////
//use Module\Admin\Controller\DepartamentoController;
//use Module\Admin\Controller\MunicipioController;
//
////
//$objetoDpto = new DepartamentoController();
//$objetMuni = new MunicipioController();
//$recordsDpto = $objetoDpto->getAssocDepto();
//$idDptoMuni = "";

//echo "<pre>";
//print_r($recordsDpto);
//echo "</pre>";
//exit();
?>

<!DOCTYPE html>
<!--
Jairo Alberto Cardenas Molina
jairo.cardenas@usantoto.edu.co
19/09/2018
-->

<html>
    <head>
        <?php
        require_once '../../../public/icono.php';
        ?>
        <link rel="shortcut icon" href="../../../public/img/logoIzquierda.ico" />
        <meta charset="UTF-8">
        <title>personaje crear(2020)</title>
        <link href="../../../public/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/fontawesome-all.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/animate.css" rel="stylesheet" type="text/css"/>


        <script src="../../../public/js/popper.js" type="text/javascript"></script>
        <script src="../../../public/js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="../../../public/js/bootstrap.js" type="text/javascript"></script>
        <script src="../../../public/js/jquery-ui.js" type="text/javascript"></script>
        <script src="../../../public/js/jquery.validate.js" type="text/javascript"></script>
    </head>

    <body class="container">
  
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="../../../public/index.php"><i class="fa fa-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Crear Personaje</li>
            </ol>
        </nav>

        <div class="row" style="padding-top: 22px">
    
            <div class="col-sm-12 ">
                <div class="jumbotron">
                    <div class="card card-outline-secondary" >
                        <div class="card-header bg-info text-white" >
                            <h3 class="mb-0" >Crear personaje</h3>
                        </div>
                        <div class="card-body">
                            <div class="alert alert-info p-2 pb-3">
                                <a class="close font-weight-normal initialism" data-dismiss="alert" href="#"><samp>&times;</samp></a> 
                                Diligencia este formulario para crear un personaje
                            </div>
                            <form id="myform" name="myform" action="../controller/personajepostcontroller.php" method="post">
                                
                          
                                <div class="form-group row ">
                                    <label class="col-sm-2 control-label " for="name">Nombre: </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Digite el nombre del personaje" >
                                    </div>
                                </div>


                                <div class="form-group row ">
                                    <label class="col-sm-2 control-label " for="name">Rol: </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="rol" name="rol" placeholder="Digite el rol del personaje" >
                                    </div>
                                </div>


                                <div class="form-group row ">
                                    <label class="col-sm-2 control-label " for="name">Habilidad: </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="Habilidad" name="Habilidad" placeholder="Digite la habilidad del personaje" >
                                    </div>
                                </div>



                                <div class="form-group row ">
                                    <label class="col-sm-2 control-label " for="lastname">Descripción: </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="des" name="des" placeholder="Digite la descripción del personaje" >
                                    </div>
                                </div>

                              

                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label "></label>
                                    <div class="col-lg-6">
                                        <button type="Submit" class="btn btn-primary">Crear Registro</button>
                                        <button type="Reset" class="btn btn-secondary">Borrar Formulario</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>                
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#myform").validate({
                    errorClass: "text-danger",
                    rules: {
                        nombre: {
                            required: true,
                            minlength: 1,
                            maxlength: 50
                        },
                        rol: {
                            required: true,
                            minlength: 1,
                            maxlength: 50
                                    //                            email: true
                        },
                        Habilidad: {
                            required: true,
                            minlength: 5,
                            maxlength: 30
                                    //                     date: true
                        },
                        des: {
                            required: true,
                            minlength: 1,
                            maxlength: 500
                                    //                            email: true
                        }
                    },
                    messages: {
                        nombre: {
                            required: "* Digite el nombre del personaje.",
                            minlength: "* Minimo 1 caracter",
                            maxlength: "* Muchos caracteres"
                        },
                        rol: {
                            required: "* Digite el rol del personaje.",
                            minlength: "* Minimo 1 caractere",
                            maxlength: "* Muchos caracteres",
                        },
                        habilidad: {
                            required: "* Por favor digite una habilidad.",
                            minlength: "* Minimo 5 caracteres",
                            maxlength: "* Muchos caracteres",
                        },
                        des: {
                            required: "* Digite la descripción del personaje.",
                            minlength: "* Minimo 5 caracteres",
                            maxlength: "* Muchos caracteres",
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass("alert-danger text-danger");
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass("alert-danger text-danger");
                    }

                });
            });
        </script>
        <footer>
            <?php
            require_once '../../../public/footer.php';
            ?> 
        </footer>

    </body>
</html>
