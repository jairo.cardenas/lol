<?php //
require_once '../../../config/loader.php';

use Module\Admin\Controller\PersonajeController;

$objeto = new PersonajeController();
$records = $objeto->getAllPersonaje();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php
        require_once '../../../public/icono.php';
        ?>
        <meta charset="UTF-8">
        <link href="../../../public/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/fontawesome-all.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/animate.css" rel="stylesheet" type="text/css"/>

        <script src="../../../public/js/popper.js" type="text/javascript"></script>
        <script src="../../../public/js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="../../../public/js/bootstrap.js" type="text/javascript"></script>
        <script src="../../../public/js/jquery-ui.js" type="text/javascript"></script>
        <script src="../../../public/js/jquery.validate.js" type="text/javascript"></script>

        <title>LOL listar</title>
    </head>

    <body  id="centro"  class="container">
       

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="../../../public/index.php"><i class="fa fa-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Listar Personajes</li>
            </ol>
        </nav>
        <div class="row ">
          
            <div class="col-sm-12">
                <div class="jumbotron">
                    <div class="card card-outline-secondary">
                        <div class="card-header bg-info text-white" >
                            <h3 class="mb-0" >Listar Personajes</h3>
                        </div>
                        <div class="card-body">
                            <?php
                            if (empty($records)) {
                                ?>
                                <div class="alert alert-warning alert-dismissible fade show">
                                    <strong>Advertencia: </strong>
                                    No hay registros
                                    <button type="button" class="close" data-dismiss="alert">
                                        <span>x</span>
                                    </button>
                                </div>
                                <?php
                            } else {
                                ?>
                                <table class="table table-primary table-bordered table-striped table-sm table-hover">
                                    <thead>
                                        <tr>
                                            <th width="25%">Nombre</th>
                                            <th width="10%">Rol</th>
                                            <th width="20%">Habilidad</th>
                                            <th width="25%">Descripción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $xhtml = "";
                                        $estado = "Activo";
                                        foreach ($records as $key => $column) {
                                            $xhtml = "<tr class=\"table-light\">";
                                            $xhtml .= "<td>{$column["nombre"]}</td>";
                                            $xhtml .= "<td>{$column["rol"]}</td>";
                                            $xhtml .= "<td>{$column["habilidad"]}</td>";
                                            $xhtml .= "<td>{$column["descripcion"]}</td>";
                                            $xhtml . "</tr>";
                                            echo $xhtml;
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer>

            <?php
            require_once '../../../public/footer.php';
            ?>
        </footer>
    </body> 

</html>
