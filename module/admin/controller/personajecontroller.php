<?php

namespace Module\Admin\Controller;

use Module\Admin\Repository\PersonajeRepository;
use Module\Admin\Model\Personaje;

class PersonajeController {

    private $_objRepository;

    public function __construct() {
        $this->_objRepository = new PersonajeRepository();
    }

    public function getAllPersonaje() {
        return $this->_objRepository->getAll();
    }

 
    public function addPersonaje(Personaje $modelo) {
        return $this->_objRepository->add($modelo);
    }

 
}

?>