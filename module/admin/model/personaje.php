<?php

namespace Module\Admin\Model;

class Personaje {

    private $_idpersonaje;
    private $_nombre;
    private $_rol;
    private $_habilidad;
    private $_descripcion;
 

    public function __construct() {
        
    }
    
    public function get_idpersonaje() {
        return $this->_idpersonaje;
    }

    public function get_nombre() {
        return $this->_nombre;
    }

    public function get_rol() {
        return $this->_rol;
    }

    public function get_habilidad() {
        return $this->_habilidad;
    }

    public function get_descripcion() {
        return $this->_descripcion;
    }

    public function set_idpersonaje($_idpersonaje) {
        $this->_idpersonaje = $_idpersonaje;
    }

    public function set_nombre($_nombre) {
        $this->_nombre = $_nombre;
    }

    public function set_rol($_rol) {
        $this->_rol = $_rol;
    }

    public function set_habilidad($_habilidad) {
        $this->_habilidad = $_habilidad;
    }

    public function set_descripcion($_descripcion) {
        $this->_descripcion = $_descripcion;
    }





}
